﻿using EmployeeManagement.Business;
using EmployeeManagement.DataAccess.DbContexts;
using EmployeeManagement.DataAccess.Services;
using Microsoft.EntityFrameworkCore;

namespace EmployeeManagement
{
    public static class ServiceRegistrationExtensions
    {
        public static IServiceCollection RegisterDataServices(
            this IServiceCollection services, IConfiguration configuration)
        {
            // add the DbContext
            services.AddDbContext<EmployeeDbContext>(options =>
                options.UseSqlite(configuration.GetConnectionString("EmployeeManagementDB")));

            // register the repository
            services.AddScoped<IEmployeeManagementRepository, EmployeeManagementRepository>();
            return services;
        }
    }
}
