using EmployeeManagement;
using EmployeeManagement.Business;
using EmployeeManagement.DataAccess.DbContexts;
using EmployeeManagement.DataAccess.Services;
using EmployeeManagement.Middleware;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();



// add HttpClient support
builder.Services.AddHttpClient<PromotionService>(client =>
{
    client.BaseAddress = new Uri(builder.Configuration["TopLevelManagementAPIRoot"]!, UriKind.Absolute);
});

// add AutoMapper for mapping between entities and viewmodels
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

// add support for Sessions (requires a store)
builder.Services.AddDistributedMemoryCache();
builder.Services.AddSession();

// add other services
builder.Services.AddScoped<IEmployeeService, EmployeeService>();
builder.Services.AddHttpClient<IPromotionService, PromotionService>(client =>
{
    client.BaseAddress = new Uri(builder.Configuration["TopLevelManagementAPIRoot"]!, UriKind.Absolute);
});
builder.Services.AddScoped<EmployeeFactory>(); 
builder.Services.RegisterDataServices(builder.Configuration);

builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
    .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme);


var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;
    var context = services.GetRequiredService<EmployeeDbContext>();
    context.Database.Migrate();
}

// Configure the HTTP request pipeline.

// custom middleware
app.UseMiddleware<EmployeeManagementSecurityHeadersMiddleware>();

app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();

app.UseAuthorization();

app.UseSession();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=EmployeeOverview}/{action=Index}/{id?}");

app.Run();
