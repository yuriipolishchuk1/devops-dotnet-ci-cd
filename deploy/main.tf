provider "aws" {
  region = "eu-north-1"
}

variable "ci_cd_ssh_pub_key" {
  description = "SSH key for CI/CD runner"
}

variable "admin_key_name" {
  description = "SSH key for admin"
}

variable "security_group_id" {
  type = string
  description = "Security group ID"
}

variable "instance_type" {
 type = string
 default = "t3.micro"
 description = "EC2 instance type"
}

variable "ami_id" {
  type = string
  default = "ami-0014ce3e52359afbd"
  description = "AMI ID"
}

module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"

  name = "Polishchuk-app-deployment"

  instance_type          = var.instance_type
  ami                    = var.ami_id
  key_name               = var.admin_key_name
  monitoring             = true
  vpc_security_group_ids = [var.security_group_id]
  user_data = templatefile("${path.module}/user_data.sh", {
    ci_cd_ssh_pub_key = var.ci_cd_ssh_pub_key
  })


  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}