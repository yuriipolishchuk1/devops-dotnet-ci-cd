#!/usr/bin/env bash

# Update and upgrade packages
apt-get update && apt-get upgrade -y

# Create group 'docker' if it doesn't exist and add 'ubuntu' user to it
if ! getent group docker > /dev/null; then
    groupadd docker
fi
usermod -aG docker ubuntu

# Create the 'gitlab' user and add to 'docker' group
useradd -m -s /bin/bash -G docker gitlab

# Install necessary packages
apt-get install -y apt-transport-https ca-certificates curl gnupg lsb-release unattended-upgrades

# Set up Docker repository and install Docker
mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update
apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin

# Enable and start Docker
systemctl enable docker
systemctl start docker

# Add the CI/CD public SSH key to the 'gitlab' user's authorized_keys
# Note: Ensure that the variable `ci_cd_ssh_pub_key` has been exported in the environment
mkdir -p /home/gitlab/.ssh
echo "${ci_cd_ssh_pub_key}" >> /home/gitlab/.ssh/authorized_keys
chown -R gitlab:gitlab /home/gitlab/.ssh
chmod 600 /home/gitlab/.ssh/authorized_keys

# Final message with system uptime
UPTIME=$(uptime -p)
echo "The system is finally up, after $UPTIME"
